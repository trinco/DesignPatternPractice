﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NSubstitute;
using DesignPatternPractice.Creational.Builder;
using NSubstitute.Core;

namespace DesignPatternPractice.Test.Creational
{
    [TestClass]
    public class BuilderTest
    {
        [TestMethod]
        public void Build_UsingMockBuilder_ReturnAnExpectedProduct()
        {
            AbstractBuilder builder = Substitute.For<AbstractBuilder>();

            builder
                .When(bd => bd.BuildPart1(Arg.Any<Product>()))
                .Do(callInfo => ((Product)callInfo.Args()[0]).Part1 = "Build.Part1");
            builder
                .When(bd => bd.BuildPart2(Arg.Any<Product>()))
                .Do(callInfo => ((Product)callInfo.Args()[0]).Part2 = "Build.Part2");

            Director direct = new Director(builder);
            Product product = direct.Build();

            builder.Received(1).BuildPart1(Arg.Any<Product>());
            builder.Received(1).BuildPart2(Arg.Any<Product>());

            Assert.AreEqual("Build.Part1", product.Part1);
            Assert.AreEqual("Build.Part2", product.Part2);
        }
    }
}
