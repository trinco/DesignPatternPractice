﻿using DesignPatternPractice.Creational.AbstractFactory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Test.Creational
{
    [TestClass]
    public class AbstractFactoryTest
    {
        [TestMethod]
        public void CreatProductA_UsingFactory1_ReturnProductA1()
        {
            AbstractFactory factory = new ConcreteFactory1();
            AbstractProductA productA = factory.CreateProductA();

            Assert.IsInstanceOfType(productA, typeof(ProductA1));
        }

        [TestMethod]
        public void CreatProductA_UsingFactory2_ReturnProductA2()
        {
            AbstractFactory factory = new ConcreteFactory2();
            AbstractProductA productA = factory.CreateProductA();

            Assert.IsInstanceOfType(productA, typeof(ProductA2));
        }

        [TestMethod]
        public void CreatProductB_UsingFactory1_ReturnProductB1()
        {
            AbstractFactory factory = new ConcreteFactory1();
            AbstractProductB ProductB = factory.CreateProductB();

            Assert.IsInstanceOfType(ProductB, typeof(ProductB1));
        }

        [TestMethod]
        public void CreatProductB_UsingFactory2_ReturnProductB2()
        {
            AbstractFactory factory = new ConcreteFactory2();
            AbstractProductB ProductB = factory.CreateProductB();

            Assert.IsInstanceOfType(ProductB, typeof(ProductB2));
        }
    }
}
