﻿using DesignPatternPractice.Creational.Singleton;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Test.Creational
{
    [TestClass]
    public class SingletonTest
    {
        [TestMethod]
        public void Singleton_GetInstanceTwice_ReturnTheSameInstance()
        {
            Singleton instance1 = Singleton.Instance;
            Singleton instance2 = Singleton.Instance;

            Assert.AreSame(instance1, instance2);
        }
    }
}
