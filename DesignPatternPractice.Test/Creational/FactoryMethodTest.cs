﻿using DesignPatternPractice.Creational.FactoryMethod;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Test.Creational
{
    [TestClass]
    public class FactoryMethodTest
    {
        [TestMethod]
        public void CreateProduct_UsingFactoryA_ReturnProductA()
        {
            AbstractFactory factory = new FactoryA();
            AbstractProduct product = factory.CreateProduct();

            Assert.IsInstanceOfType(product, typeof(ProductA));
        }

        [TestMethod]
        public void CreateProduct_UsingFactoryB_ReturnProductB()
        {
            AbstractFactory factory = new FactoryB();
            AbstractProduct product = factory.CreateProduct();

            Assert.IsInstanceOfType(product, typeof(ProductB));
        }
    }
}
