﻿using DesignPatternPractice.Creational.Prototype;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Test.Creational
{
    [TestClass]
    public class PrototypeTest
    {
        [TestMethod]
        public void Clone_CloneTwoInstance_ReturnDifferentInstances()
        {
            Product product = Product.CreateFromPrototype();
            Product anotherProduct = (Product)product.Clone();

            Assert.AreNotSame(product, anotherProduct);
            Assert.AreNotSame(product.ListProperty, anotherProduct.ListProperty);
        }
    }
}
