﻿using DesignPatternPractice.Structural.Adapter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Test.Structural
{
    [TestClass]
    public class AdapterTest
    {
        [TestMethod]
        public void AdapterRequest_UsingMockAdaptee_RunMockAdapteeSpecialRequest()
        {
            IAdaptee adaptee = Substitute.For<IAdaptee>();

            Adapter adapter = new Adapter(adaptee);
            adapter.Request();

            adaptee.Received(1).SpecialRequest();
        }
    }
}
