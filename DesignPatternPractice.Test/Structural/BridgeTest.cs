﻿using DesignPatternPractice.Structural.Bridge;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Test.Structural
{
    [TestClass]
    public class BridgeTest
    {
        [TestMethod]
        public void BridgeOperation_UsingMockImp_RunMockImpOnce()
        {
            IImplementor imp = Substitute.For<IImplementor>();
            RedefinedAbstraction reAbstraction = new RedefinedAbstraction(imp);
            reAbstraction.Operation();

            imp.Received(1).Imp();
        }
    }
}
