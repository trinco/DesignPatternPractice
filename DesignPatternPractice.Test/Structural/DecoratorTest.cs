﻿using DesignPatternPractice.Structural.Decorator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Test.Structural
{
    [TestClass]
    public class DecoratorTest
    {
        [TestMethod]
        public void DecoratorOperation_UsingMockComponent_RunMockComponentOnce()
        {
            Component component = Substitute.For<Component>();

            Component decorator = new DecoratorA(component);

            decorator.Operation();

            component.Received(1).Operation();
        }
    }
}
