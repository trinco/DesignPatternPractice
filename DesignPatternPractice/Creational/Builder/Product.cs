﻿using System;

namespace DesignPatternPractice.Creational.Builder
{
    public class Product
    {
        public string Part1 { get; set; }
        public string Part2 { get; set; }
        
        public void Show()
        {
            Console.WriteLine("++++++++++ Product ++++++++++");
            Console.WriteLine(Part1);
            Console.WriteLine(Part2);
            Console.WriteLine("++++++++++ End ++++++++++");
        }
    }
}