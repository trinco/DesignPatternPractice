﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Creational.Builder
{
    public abstract class AbstractBuilder
    {
        public abstract void BuildPart1(Product product);
        public abstract void BuildPart2(Product product);
    }
}