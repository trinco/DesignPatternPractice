﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Creational.Builder
{
    public class Builder1 : AbstractBuilder
    {
        public override void BuildPart1(Product product)
        {
            product.Part1 = "Build1.Part1";
        }

        public override void BuildPart2(Product product)
        {
            product.Part2 = "Build1.Part2";
        }
    }
}