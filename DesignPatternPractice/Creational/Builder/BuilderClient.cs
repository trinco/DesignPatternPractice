﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Creational.Builder
{
    /// <summary>
    /// Builder（建造者模式）
    /// 意图：Separate the construction of a complex object from its representation so that the same construction process can create different representations.
    ///      将一个复杂对象的构建与它的表示分离，使得同样的构建过程可以创建不同的表示。
    /// 适用：单一产品，各部件有不同实现的延迟构造，由Director固化构造流程。
    /// 关注点：单一产品内部细节的多种实现方式。
    /// </summary>
    public class BuilderClient
    {
        public void Show()
        {
            Director direct1 = new Director(new Builder1());
            Director direct2 = new Director(new Builder2());

            Product product1 = direct1.Build();
            Product product2 = direct2.Build();

            Console.WriteLine("---------- Builder ----------");
            product1.Show();
            product2.Show();
            Console.WriteLine();
        }
    }
}
