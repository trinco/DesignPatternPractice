﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Creational.Builder
{
    public class Builder2 : AbstractBuilder
    {
        public override void BuildPart1(Product product)
        {
            product.Part1 = "Build2.Part1";
        }

        public override void BuildPart2(Product product)
        {
            product.Part2 = "Build2.Part2";
        }
    }
}