﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Creational.Builder
{
    public class Director
    {
        private AbstractBuilder _builder;

        public AbstractBuilder Builder
        {
            get
            {
                return _builder;
            }

            private set
            {
                _builder = value;
            }
        }

        public Director(AbstractBuilder builder)
        {
            Builder = builder;
        }

        public Product Build()
        {
            Product product = new Product();

            Builder.BuildPart1(product);
            Builder.BuildPart2(product);

            return product;
        }
    }
}