﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Creational.FactoryMethod
{
    public class ProductA : AbstractProduct
    {
        public override void Show()
        {
            Console.WriteLine("ProductA");
        }

        public override string ToString()
        {
            return "ProductA";
        }
    }
}