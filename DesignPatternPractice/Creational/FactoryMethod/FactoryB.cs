﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Creational.FactoryMethod
{
    public class FactoryB : AbstractFactory
    {
        public override AbstractProduct CreateProduct()
        {
            return new ProductB();
        }
    }
}