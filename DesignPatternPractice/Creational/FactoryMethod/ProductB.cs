﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Creational.FactoryMethod
{
    public class ProductB : AbstractProduct
    {
        public override void Show()
        {
            Console.WriteLine("ProductB");
        }

        public override string ToString()
        {
            return "ProductB";
        }
    }
}