﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Creational.FactoryMethod
{
    public abstract class AbstractProduct
    {
        public abstract void Show();
    }
}