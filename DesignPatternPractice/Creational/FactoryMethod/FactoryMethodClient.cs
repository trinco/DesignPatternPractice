﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Creational.FactoryMethod
{
    /// <summary>
    /// Factory Method（工厂方法模式）
    /// 意图：Define an interface for creating an object, but let subclasses decide which class to instantiate.
    ///      Factory Method lets a class defer instantiation to subclasses.
    ///      定义一个用于创建目标对象的接口，让子类决定实例化哪一个目标类。
    ///      Factory Method 使一个类的实例化延迟到其子类。
    /// 适用：将产品生成延迟到子类。
    /// 关注点：延迟生成。
    /// </summary>
    public class FactoryMethodClient
    {
        public void Show()
        {
            AbstractFactory factoryA = new FactoryA();
            AbstractFactory factoryB = new FactoryB();

            AbstractProduct productA = factoryA.CreateProduct();
            AbstractProduct productB = factoryB.CreateProduct();

            Console.WriteLine("---------- FactoryMethod ----------");
            productA.Show();
            productB.Show();
            Console.WriteLine();
        }
    }
}
