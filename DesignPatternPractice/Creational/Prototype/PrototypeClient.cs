﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Creational.Prototype
{
    /// <summary>
    /// Prototype（原型模式）
    /// 意图：Specify the kinds of objects to create using a prototypical instance, and create new objects by copying this prototype.
    ///      用原型实例指定创建对象的种类，并且通过拷贝这些原型创建新的对象。
    /// 适用：通过从原型克隆的方式生成对象，简化生成过程，并提供默认值。
    /// 关注点：克隆。
    /// 注意：浅复制和深复制
    /// </summary>
    public class PrototypeClient
    {
        public void Show()
        {
            Product product1 = Product.CreateFromPrototype();
            Product product2 = (Product)product1.Clone();

            product2.IntProperty = 11;
            product2.StringProperty = "Ok";
            product2.ListProperty[1] = 4;

            Console.WriteLine("---------- Prototype ----------");
            product1.Show();
            product2.Show();
            Console.WriteLine();
        }
    }
}
