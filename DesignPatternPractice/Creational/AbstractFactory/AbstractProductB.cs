﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Creational.AbstractFactory
{
    public abstract class AbstractProductB
    {
        public abstract void Show();
    }
}