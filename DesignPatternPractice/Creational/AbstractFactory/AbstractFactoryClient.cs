﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Creational.AbstractFactory
{
    /// <summary>
    /// AbstractFactory（抽象工厂模式）
    /// 意图：Provide an interface for creating families of related or dependent objects without specifying their concrete classes.
    ///      提供一个创建一系列相关或相互依赖对象的接口，而无需指定它们具体的类。
    /// 适用：多产品延迟分离生成。
    /// 关注点：多种产品的多种生成实现方式。
    /// </summary>
    public class AbstractFactoryClient
    {
        public void Show()
        {
            AbstractFactory factory1 = new ConcreteFactory1();
            AbstractFactory factory2 = new ConcreteFactory2();

            AbstractProductA productA1 = factory1.CreateProductA();
            AbstractProductA productA2 = factory2.CreateProductA();

            AbstractProductB productB1 = factory1.CreateProductB();
            AbstractProductB productB2 = factory2.CreateProductB();

            Console.WriteLine("---------- Abstract Factory ----------");
            productA1.Show();
            productA2.Show();
            productB1.Show();
            productB2.Show();
            Console.WriteLine();
        }
    }
}