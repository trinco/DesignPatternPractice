﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Creational.AbstractFactory
{
    public class ProductB1 : AbstractProductB
    {
        public override void Show()
        {
            Console.WriteLine("ProductB1");
        }
    }
}