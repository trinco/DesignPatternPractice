﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Creational.AbstractFactory
{
    public class ProductA1 : AbstractProductA
    {
        public override void Show()
        {
            Console.WriteLine("ProductA1");
        }
    }
}