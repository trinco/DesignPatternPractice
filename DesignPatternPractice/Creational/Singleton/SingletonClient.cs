﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Creational.Singleton
{
    /// <summary>
    /// Singleton（单例模式）
    /// 意图：Ensure a class only has one instance, and provide a global point of access to it.
    ///      保证一个类仅有一个实例，并提供一个访问它的全局访问点。
    /// 适用：只能有一个实例的场景。
    /// 关注点：在多线程下，采用延迟生成实例时，需要考虑同步问题，可采用双锁机制；也可以通过初始化就生成的方式避免同步问题。
    /// </summary>
    public class SingletonClient
    {
        public void Show()
        {
            Singleton obj1 = Singleton.Instance;
            Singleton obj2 = Singleton.Instance;

            Console.WriteLine("---------- Singleton ----------");
            Console.WriteLine("obj1 and obj2 " + (obj1 == obj2 ? "are " : "are not ") + "same.");
            Console.WriteLine();
        }
    }
}
