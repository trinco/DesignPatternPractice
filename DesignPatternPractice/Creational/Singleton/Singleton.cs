﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Creational.Singleton
{
    public class Singleton
    {
        private static Singleton _instance;
        private static readonly object _syncObj = new object();

        protected Singleton() { }

        public static Singleton Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock(_syncObj)
                    {
                        if(_instance == null)
                        {
                            _instance = new Singleton();
                        }
                    }
                }
                return _instance;
            }
        }
    }
}
