﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("========== Creational ==========");

            Creational.AbstractFactory.AbstractFactoryClient abstractFactoryClient = new Creational.AbstractFactory.AbstractFactoryClient();
            abstractFactoryClient.Show();

            Creational.Builder.BuilderClient builderClient = new Creational.Builder.BuilderClient();
            builderClient.Show();

            Creational.FactoryMethod.FactoryMethodClient factoryMethodClient = new Creational.FactoryMethod.FactoryMethodClient();
            factoryMethodClient.Show();

            Creational.Prototype.PrototypeClient prototypeClient = new Creational.Prototype.PrototypeClient();
            prototypeClient.Show();

            Creational.Singleton.SingletonClient singletonClient = new Creational.Singleton.SingletonClient();
            singletonClient.Show();

            Console.WriteLine("========== Creational ==========");

            Structural.Adapter.AdapterClient adapterClient = new Structural.Adapter.AdapterClient();
            adapterClient.Show();

            Structural.Bridge.BridgeClient bridgeClient = new Structural.Bridge.BridgeClient();
            bridgeClient.Show();

            Structural.Composite.CompositeClient compositeClient = new Structural.Composite.CompositeClient();
            compositeClient.Show();

            Structural.Decorator.DecoratorClient decoratorClient = new Structural.Decorator.DecoratorClient();
            decoratorClient.Show();

            Console.ReadKey();
        }
    }
}
