﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Structural.Adapter
{
    /// <summary>
    /// Adapter（适配器模式）
    /// 意图：Convert the interface of a class into another interface clients expect.
    ///      Adapter lets classes work together that couldn't otherwise because of incompatible interfaces.
    ///      将一个类的接口转换成客户希望的另外一个接口。
    ///      Adapter 模式使得原本由于接口不兼容而不能一起工作的那些类可以一起工作。
    /// 适用：Client通过ITarget接口间接调用IAdaptee接口的实现，使得Client和Adaptee可以粘合而不需要改变。
    /// 说明1：将Adaptee抽象成接口，便于应用单元测试，并可适配所有继承自该接口的类。
    /// 说明2：从个人观点来看，Bridge和Adapter模式其实是一个模式在不同场景中的两种应用，从抽象角度来看，可以归并为一个模式。
    /// </summary>
    public class AdapterClient
    {
        public void Show()
        {
            Adaptee adaptee = new Adaptee();
            Adapter adapter = new Adapter(adaptee);

            Console.WriteLine("---------- Adapter ----------");
            adapter.Request();
            Console.WriteLine();
        }
    }
}
