﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Structural.Adapter
{
    public class Adaptee : IAdaptee
    {
        public void SpecialRequest()
        {
            Console.WriteLine("Adaptee");
        }
    }
}