﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Structural.Adapter
{
    public interface ITarget
    {
        void Request();
    }
}
