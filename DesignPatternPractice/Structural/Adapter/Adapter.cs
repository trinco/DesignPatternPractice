﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Structural.Adapter
{
    public class Adapter : ITarget
    {
        public IAdaptee Adaptee { get; private set; }

        public Adapter(IAdaptee adaptee)
        {
            Adaptee = adaptee;
        }

        public void Request()
        {
            Console.WriteLine("+++++ Run in adapter +++++");
            Adaptee.SpecialRequest();
            Console.WriteLine("+++++ End +++++");
        }
    }
}