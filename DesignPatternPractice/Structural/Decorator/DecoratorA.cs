﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Structural.Decorator
{
    public class DecoratorA : Decorator
    {
        public DecoratorA(Component component) : base(component) { }

        public override void Operation()
        {
            Console.WriteLine("+++++ Do Decorate A +++++");
            Component.Operation();
            Console.WriteLine("+++++ End Do Decorate A +++++");
        }
    }
}