﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Structural.Decorator
{
    public abstract class Decorator : Component
    {
        public Component Component { get; private set; }

        public Decorator(Component component)
        {
            Component = component;
        }
    }
}