﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Structural.Decorator
{
    public class ComponentA : Component
    {
        public override void Operation()
        {
            Console.WriteLine("ComponentA");
        }
    }
}