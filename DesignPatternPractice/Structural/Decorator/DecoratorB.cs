﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Structural.Decorator
{
    public class DecoratorB : Decorator
    {
        public DecoratorB(Component component) : base(component) { }

        public override void Operation()
        {
            Console.WriteLine("+++++ Do Decorate B +++++");
            Component.Operation();
            Console.WriteLine("+++++ End Do Decorate B +++++");
        }
    }
}