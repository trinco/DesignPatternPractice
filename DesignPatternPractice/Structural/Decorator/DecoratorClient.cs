﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Structural.Decorator
{
    /// <summary>
    /// Decorator（装饰模式）
    /// 意图：Attach additional responsibilities to an object dynamically.
    ///      Decorators provide a flexible alternative to subclassing for extending functionality.
    ///      动态地给一个对象添加一些额外的职责。
    ///      就增加功能来说，Decorator 模式相比生成子类更为灵活。
    /// 适用：Decorator包含一个被包装对象的引用，用于从外部修改被包装对象的行为。
    /// </summary>
    public class DecoratorClient
    {
        public void Show()
        {
            Component componentA = new ComponentA();
            Component decoratorA = new DecoratorA(componentA);
            Component decoratorB = new DecoratorB(componentA);

            Console.WriteLine("---------- Decorator ----------");

            componentA.Operation();
            decoratorA.Operation();
            decoratorB.Operation();

            Console.WriteLine();
        }
    }
}
