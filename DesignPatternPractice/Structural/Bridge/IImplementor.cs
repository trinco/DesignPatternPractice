﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Structural.Bridge
{
    public interface IImplementor
    {
        void Imp();
    }
}