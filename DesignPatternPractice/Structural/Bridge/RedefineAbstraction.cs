﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Structural.Bridge
{
    public class RedefinedAbstraction : Abstraction
    {
        public RedefinedAbstraction(IImplementor imp) : base(imp) { }

        public override void Operation()
        {
            Console.WriteLine("+++++ Run in RedefinedAbstraction +++++");
            IImplementor.Imp();
            Console.WriteLine("+++++ End +++++");
        }
    }
}