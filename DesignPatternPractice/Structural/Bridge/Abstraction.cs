﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Structural.Bridge
{
    public class Abstraction
    {
        public IImplementor IImplementor
        {
            get;
            private set;
        }

        public Abstraction(IImplementor imp)
        {
            IImplementor = imp;
        }

        public virtual void Operation()
        {
            Console.WriteLine("+++++ Run in Abstraction +++++");
            IImplementor.Imp();
            Console.WriteLine("+++++ End +++++");
        }
    }
}