﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Structural.Bridge
{
    public class ImplementorB : IImplementor
    {
        public void Imp()
        {
            Console.WriteLine("This is Imp B");
        }
    }
}