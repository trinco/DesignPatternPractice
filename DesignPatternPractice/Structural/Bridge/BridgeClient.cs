﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Structural.Bridge
{
    /// <summary>
    /// Bridge（桥接模式）
    /// 意图：Decouple an abstraction from its implementation so that the two can vary independently.
    ///      将抽象部分与它的实现部分分离，使它们都可以独立地变化。
    /// 适用：分离基本操作和基于这些基本操作的复合操作。
    /// 说明：从个人观点来看，Bridge和Adapter模式其实是一个模式在不同场景中的两种应用，从抽象角度来看，可以归并为一个模式。
    /// </summary>
    public class BridgeClient
    {
        public void Show()
        {
            ImplementorA impA = new ImplementorA();
            ImplementorB impB = new ImplementorB();

            Abstraction abstractionA = new Abstraction(impA);
            Abstraction abstractionB = new Abstraction(impB);

            RedefinedAbstraction reAbstractionA = new RedefinedAbstraction(impA);
            RedefinedAbstraction reAbstractionB = new RedefinedAbstraction(impB);

            Console.WriteLine("---------- Bridge ----------");

            abstractionA.Operation();
            abstractionB.Operation();
            reAbstractionA.Operation();
            reAbstractionB.Operation();

            Console.WriteLine();
        }
    }
}
