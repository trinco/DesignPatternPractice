﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Structural.Composite
{
    public class Leaf : Component
    {
        public Leaf(string name) : base(name) { }

        public override void Operation()
        {
            ShowBlank();
            Console.WriteLine(Name);
        }

        public override void Add(Component component)
        {
            throw new NotImplementedException("Cannot Add");
        }

        public override void Remove(Component component)
        {
            throw new NotImplementedException("Cannot Remove");
        }
    }
}