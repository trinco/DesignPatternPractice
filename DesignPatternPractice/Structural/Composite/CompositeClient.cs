﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Structural.Composite
{
    /// <summary>
    /// Composite（组合模式）
    /// 意图：Compose objects into tree structures to represent part-whole hierarchies.
    ///      Composite lets clients treat individual objects and compositions of objects uniformly.
    ///      将对象组合成树形结构以表示 “部分-整体” 的层次结构。
    ///      Composite 使得用户对于单个对象和组合对象的使用具有一致性。
    /// 适用：统一运行一批可以被组织成树形结构的对象，也可以单独运行其中一部分或一个对象。
    /// </summary>
    public class CompositeClient
    {
        public void Show()
        {
            Composite root = new Composite("root");
            Composite composite1 = new Composite("composite1");
            Composite composite2 = new Composite("composite2");
            Composite composite3 = new Composite("composite3");

            Leaf leaf1 = new Leaf("leaf1");
            Leaf leaf2 = new Leaf("leaf2");
            Leaf leaf3 = new Leaf("leaf3");
            Leaf leaf4 = new Leaf("leaf4");

            root.Add(leaf1);
            root.Add(composite1);
            composite1.Add(leaf2);
            composite1.Add(composite2);
            composite2.Add(leaf3);
            composite2.Add(composite3);
            root.Add(leaf4);

            Console.WriteLine("---------- Composite ----------");

            root.Operation();

            Console.WriteLine();
        }
    }
}
