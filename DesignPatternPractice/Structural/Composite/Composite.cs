﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternPractice.Structural.Composite
{
    public class Composite : Component
    {
        public Composite(string name) : base(name) { }

        public override void Operation()
        {
            ShowBlank();
            Console.WriteLine(Name);
            foreach (var child in Children)
            {
                child.Operation();
            }
        }
    }
}